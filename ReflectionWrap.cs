﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Reflection
{
    class ReflectionWrap
    {
        internal object GetObjectFromPrivateMethod<T>(string methodName)
        {
            return typeof(T).InvokeMember(methodName,
                BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.InvokeMethod,
                null,
                null,
                null);
        }

        internal string SerializePrivateFields<T>(T data)
        {
            return string.Join("\n",
                data.GetType()
                    .GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Select(s => $"{s.Name}: {s.GetValue(data)}"));
        }
        internal string SerializePrivateFieldsTest<T>(T obj, int iterations)
        {
            var result = string.Empty;
            for (var i = 0; i < iterations; i++)
            {
                result = SerializePrivateFields(obj);
            }

            return result;
        }

        internal T DeserializeFromString<T>(string reflectFile)
        {
            var fields = reflectFile.Split("\n");

            var instance = Activator.CreateInstance<T>();
            foreach (var field in fields)
            {
                var keyValue = field.Replace(" ", "").Split(":");
                typeof(T).GetField(keyValue[0], BindingFlags.NonPublic | BindingFlags.Instance).SetValue(instance, int.Parse(keyValue[1]));
            }
            return instance;
        }
    }
}
