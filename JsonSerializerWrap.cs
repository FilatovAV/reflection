﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reflection
{
    class JsonSerializerWrap
    {
        private readonly JsonSerializerSettings _jsSettings = new JsonSerializerSettings();
        private readonly DefaultContractResolver _contractResolver = new DefaultContractResolver();
        public JsonSerializerWrap()
        {
            _contractResolver.DefaultMembersSearchFlags |= System.Reflection.BindingFlags.NonPublic;
            _jsSettings.ContractResolver = _contractResolver;
        }

        internal string Serialize<T>(T data)
        {
            return JsonConvert.SerializeObject(data, _jsSettings);
        }

        internal string SerializeTest<T>(T data, int Iterations)
        {
            var json = string.Empty;
            for (int i = 0; i < Iterations; i++)
            {
                json = Serialize(data);
            }
            return json;
        }

        internal T Deserialize<T>(string dataString)
        {
            return JsonConvert.DeserializeObject<T>(dataString, _jsSettings);
        }
    }
}
