﻿using System;
using System.Diagnostics;
using System.IO;

namespace Reflection
{
    class Program
    {
        private const int Iterations = 10000;
        private static readonly Stopwatch _timer = new Stopwatch();
        private static Data _data;
        static void Main(string[] args)
        {
            ReflectionTest();
            JsonSerializeTest();
            Console.ReadLine();
        }

        private static void ReflectionTest()
        {
            Console.WriteLine("Создание экземпляра типа, путем вызова приватного метода типа.");

            var reflector = new ReflectionWrap();
            _data = (Data)reflector.GetObjectFromPrivateMethod<Data>("Get");

            Console.WriteLine($"Запуск сериализации средствами Reflection {Iterations} итераций.");

            _timer.Start();
            var serializeString = reflector.SerializePrivateFieldsTest(_data, Iterations);
            _timer.Stop();

            Console.WriteLine($"Потраченное время: {_timer.ElapsedMilliseconds}  мс.");
            Console.WriteLine(serializeString);
            Console.WriteLine("Сохранение данных в reflect.txt");

            File.WriteAllText("reflect.txt", serializeString);

            Console.WriteLine("Чтение из файла.");

            var reflectFile = File.ReadAllText("reflect.txt");

            Console.WriteLine("Десериализация данных в новый объект.");

            _timer.Restart();
            var deserializeData = reflector.DeserializeFromString<Data>(reflectFile);
            _timer.Stop();

            Console.WriteLine($"Десериализация данных в новый объект заняла - {_timer.Elapsed} мс.");
        }

        private static void JsonSerializeTest()
        {
            //------------------------------------------------------Json-------------------------------------------------------------------------------
            Console.WriteLine($"\n\nЗапуск сериализации средствами Newtonsoft.Json.Serialization {Iterations} итераций.");
            _timer.Restart();

            var jsonSer = new JsonSerializerWrap();
            var jsonStr = jsonSer.SerializeTest(_data, Iterations);

            _timer.Stop();
            Console.WriteLine($"Потраченное время: {_timer.ElapsedMilliseconds} мс.");
            Console.WriteLine(jsonStr);

            Console.WriteLine("Сохранение данных в json.txt");
            File.WriteAllText("json.txt", jsonStr);

            Console.WriteLine("Чтение из файла.");

            var jsonString = File.ReadAllText("json.txt");

            Console.WriteLine("Десериализация данных в новый объект.");

            _timer.Restart();
            var deserializeJsonData = jsonSer.Deserialize<Data>(jsonString);
            _timer.Stop();

            Console.WriteLine($"Десериализация данных в новый объект заняла - {_timer.ElapsedMilliseconds} мс.");
        }
    }
}
